import axios from "axios";

function Sleep(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

/* Helper Class
 * To make requesting countries and regions more flexible
 * */
class MapData {
    private readonly country: string;

    constructor(country: string = "countries/us/us-all") {
        this.country = country;
    }

    /* Helper Function
     * To get the countries of a specific region
     * */
    getWorld = async () => {
        await Sleep(3000);
        return await axios.get("https://code.highcharts.com/mapdata/" + this.country + ".geo.json");
    };
}

export default MapData;
