import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from 'react-router-dom';
import Plotly from './plot-libs/Plotly';
import Recharts from './plot-libs/Recharts';
import Visx from './plot-libs/Visx';
import ReactVis from './plot-libs/ReactVis';
import CanvasJs from './plot-libs/CanvasJs';
import Victory from './plot-libs/Victory';
import Highchart from './plot-libs/Highchart';


function About() {
    return <h2>This is my IDP Chart Libs Test Repo</h2>;
}

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <nav>
                        <ul>
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/plotly">Plotly</Link>
                            </li>
                            <li>
                                <Link to="/recharts">Recharts</Link>
                            </li>
                            {/*<li>
                                <Link to="/visx">Visx</Link>
                            </li>*/}
                            {/*<li>

                                <Link to="/react-vis">React-vis</Link>
                            </li>*/}
                            {/*<li>
                                <Link to="/victory">Victory</Link>
                            </li>*/}
                            <li>
                                <Link to="/canvas">CanvasJS</Link>
                            </li>
                            <li>
                                <Link to="/highcharts">Highcharts</Link>
                            </li>
                        </ul>
                    </nav>

                    {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                    <Switch>
                        <Route path="/plotly">
                            <Plotly/>
                        </Route>
                        <Route path="/recharts">
                            <Recharts/>
                        </Route>
                        <Route path="/visx">
                            <Visx/>
                        </Route>
                        <Route path="/react-vis">
                            <ReactVis/>
                        </Route>
                        <Route path="/victory">
                            <Victory/>
                        </Route>
                        <Route path="/canvas">
                            <CanvasJs/>
                        </Route>
                        <Route path="/highcharts">
                            <Highchart/>
                        </Route>
                        <Route path="/">
                            <About/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}


export default App;
