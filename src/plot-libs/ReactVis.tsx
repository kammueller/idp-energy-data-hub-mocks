import React from "react";
import '../../node_modules/react-vis/dist/style.css'; // TODO we need this!!
import {curveCatmullRom} from 'd3-shape';
import {
    XYPlot,
    LineSeries,
    VerticalGridLines,
    HorizontalGridLines,
    YAxis,
    XAxis,
    MarkSeries,
    VerticalBarSeries,
    ChartLabel, AreaSeries, DiscreteColorLegend,
} from 'react-vis';

type ReactVisProps = {};

type ReactVisState = {};

class ReactVis extends React.Component<ReactVisProps, ReactVisState> {
    render() {
        const series1 = [
            {x: 0, y: 8},
            {x: 1, y: 5},
            {x: 2, y: 4},
            {x: 3, y: 9},
            {x: 4, y: 1},
            {x: 5, y: 7},
            {x: 6, y: 6},
            {x: 7, y: 3},
            {x: 8, y: 2},
            {x: 11, y: 0.1},
        ];
        const series2 = [
            {x: 0, y: 5},
            {x: 1, y: 4},
            {x: 2, y: 9},
            {x: 3, y: 1},
            {x: 4, y: 7},
            {x: 5, y: 6},
            {x: 6, y: 3},
            {x: 7, y: 2},
            {x: 8, y: 0.1},
            {x: 11, y: 8},
        ];
        const series2_conf = [
            {x: 0, y: 5, y0: 5},
            {x: 1, y: 4, y0: 4},
            {x: 2, y: 9, y0: 9},
            {x: 3, y: 2, y0: 0},
            {x: 4, y: 8, y0: 6},
            {x: 5, y: 7, y0: 5},
            {x: 6, y: 4, y0: 2},
            {x: 7, y: 3, y0: 1},
            {x: 8, y: 1, y0: 0},
            {x: 11, y: 10, y0: 6},
        ];
        // this color setting does not work for line series. it's all quite stupid.
        const series3 = [
            {x: 0, y: 8, color: 8},
            {x: 1, y: 5, color: 5},
            {x: 2, y: 4, color: 4},
            {x: 3, y: 9, color: 9},
            {x: 4, y: 1, color: 1},
            {x: 5, y: 8, color: 8},
            {x: 6, y: 5, color: 5},
            {x: 7, y: 4, color: 4},
            {x: 8, y: 9, color: 9},
            {x: 11, y: 1, color: 1},
        ];

        return (
            <div>
                <XYPlot height={400} width={1200} yDomain={[0, 10]}>
                    <VerticalGridLines/>
                    <HorizontalGridLines/>
                    <XAxis/>
                    <ChartLabel
                        text="X Axis"
                        className="alt-x-label"
                        includeMargin={false}
                        xPercent={0.01}
                        yPercent={1.01}
                    />
                    <YAxis/>
                    <ChartLabel
                        text="Y Axis"
                        className="alt-y-label"
                        includeMargin={false}
                        xPercent={0.01}
                        yPercent={0.06}
                        style={{
                            transform: 'rotate(-90)',
                            textAnchor: 'end',
                        }}
                    />
                    <DiscreteColorLegend items={[
                        {title: 'Some Data', color: 'black', strokeStyle: 'dashed'},
                        {title: 'Some interpolated Data', color: '#8884d8'},
                        {title: 'Numbers', color: 'rgba(130,202,157,1)'},
                        {title: 'Numbers - 95% conf.int.', color: 'rgba(130,202,157,0.5)', strokeWidth: 13},
                    ]} orientation={'horizontal'}/>

                    <LineSeries data={series1} color={'black'} strokeStyle={'dashed'}/>
                    <LineSeries data={series1} color={'#8884d8'} curve={curveCatmullRom.alpha(0.5)}/>

                    <AreaSeries
                        color={'rgba(130,202,157,0.5)'}
                        stroke={'none'}
                        data={series2_conf}
                        curve={'curveMonotoneX'}
                    />
                    <LineSeries data={series2} color={'rgba(130,202,157,1)'} curve={'curveMonotoneX'}/>
                </XYPlot>
                <XYPlot height={200} width={300} xType={'ordinal'} colorType={'category'}
                        colorRange={['black', 'red', 'white']} colorDomain={[0, 5, 10]}>
                    <VerticalBarSeries data={series1} barWidth={0.8} color={0}/>
                    <VerticalBarSeries data={series2} barWidth={0.8} color={0}/>
                    <VerticalBarSeries data={series3} barWidth={0.8}/>
                </XYPlot>
                <XYPlot height={200} width={300} colorType={'category'}>
                    <MarkSeries data={series1}/>
                    <MarkSeries data={series2}/>
                    <MarkSeries data={series3}/>
                </XYPlot>
                <XYPlot height={200} width={300} xDomain={[9, 0]} colorType={'category'}>
                    <MarkSeries data={series3}/>
                </XYPlot>

            </div>
        );
    }
}

export default ReactVis;
