import React from "react";
import Plot from "react-plotly.js";
import {BigSeriesCountry} from './utils';

type PlotlyBigProps = {
    data: BigSeriesCountry[];
};

type PlotlyBigState = {
    interpolation: number;
    data: any[];
    layout: any;
    frames: any[];
    config: any;
};

class PlotlyBig extends React.Component<PlotlyBigProps, PlotlyBigState> {

    constructor(props: Readonly<PlotlyBigProps> | PlotlyBigProps) {
        super(props);
        this.state = {
            interpolation: 0,
            data: this.props.data.map((country) => ({
                type: 'scatter', // or scattergl
                mode: 'lines',
                line: {shape: 'spline'},
                name: country.who,
                x: country.values.map(v => v.when),
                y: country.values.map(v => v.what[0]),
            })),
            layout: {
                title: 'Many Data Points',
                legend: {
                    orientation: 'h',
                    y: -0.2,
                },
                yaxis: {automargin: true}, // definitely set that!
                xaxis: {showticklabels: true},
                margin: {pad: 0},
            },
            frames: [],
            config: {
                scrollZoom: true,
                displaylogo: false,
                toImageButtonOptions: {
                    format: 'svg',
                    scale: 2,
                },
                responsive: true,
            },
        };
    }

    changeBigData = (e: any) => {
        const newVal = e.target.value;
        const interpolated = this.state.data.map((dataSet, index) => ({
            ...dataSet,
            y: this.props.data[index].values.map(v =>
                (100 - newVal) / 100 * v.what[0] + (newVal) / 100 * v.what[1],
            ),
        }));
        this.setState({interpolation: newVal, data: interpolated});
    };

    render() {
        return (
            <div>
                <input type={'number'} min={0} max={100} step={5}
                       value={this.state.interpolation} onChange={this.changeBigData}/>
                <Plot
                    style={{width: '100%', height: 400}}
                    data={this.state.data}
                    layout={this.state.layout}
                    frames={this.state.frames}
                    config={this.state.config}
                    onInitialized={(figure: any) => this.setState(figure)}
                    onUpdate={(figure: any) => this.setState(figure)}
                />
            </div>
        );
    }
}

export default PlotlyBig;
