import React from 'react';
import Highmaps from 'highcharts/highmaps';
import HighchartsReact from 'highcharts-react-official';
import MapData from "../api/MapData";
import Highcharts from 'highcharts';
import '../libs/pro4j';

require('highcharts/modules/map')(Highcharts);

// https://jsfiddle.net/wchmiel/0ckejhtw/
(function (H) {
    var merge = H.merge,
        Point = H.Point,
        seriesType = H.seriesType;

    seriesType('mappolygon', 'polygon', {}, {}, {
        applyOptions: function (options: any, x: any) {
            var mergedOptions = (
                options.lat !== undefined &&
                options.lon !== undefined ?
                    merge(options, this.series.chart.fromLatLonToPoint(options)) :
                    options
            );
            // @ts-ignore
            return Point.prototype.applyOptions.call(this, mergedOptions, x);
        },
    });
}(Highcharts));

type HighchartMapProps = {
    subplotSize: number;
};

type HighchartMapState = {
    usData: any;
    gbData: any;
    caData: any;
    heatOpt?: Highmaps.Options;
    bubbleOpt?: Highmaps.Options;
    polyOpt?: Highmaps.Options;
};

// from https://codesandbox.io/s/highcharts-react-demo-jmu5h?file=/components/Map.jsx

class HighchartMap extends React.Component<HighchartMapProps, HighchartMapState> {
    usMap: MapData;
    gbMap: MapData;
    caMap: MapData;
    heatOpt_prepare: Highmaps.Options;
    bubbleOpt_prepare: Highmaps.Options;
    polyOpt_prepare: Highmaps.Options;

    constructor(props: Readonly<HighchartMapProps> | HighchartMapProps) {
        super(props);
        this.state = {
            usData: null,
            gbData: null,
            caData: null,
        };
        // init to get the map data from api
        this.usMap = new MapData();
        this.gbMap = new MapData('countries/gb/gb-all');
        this.caMap = new MapData('countries/ca/ca-qc-all');
        // preparing the config of map with empty data
        this.heatOpt_prepare = {
            title: {
                text: "Widget click by location",
            },
            chart: {
                backgroundColor: "transparent",
                type: "map",
                map: undefined, // will be filled later
            },
            mapNavigation: {
                enabled: true,
                enableButtons: false,
            },
            colorAxis: {
                min: 1,
                // type: 'logarithmic',
                minColor: '#EEEEFF',
                maxColor: '#000022',
                stops: [
                    [0, '#EFEFFF'],
                    [0.67, '#4444FF'],
                    [1, '#000022'],
                ],
            },
            tooltip: {
                pointFormatter: function () {
                    return this.name + ": " + this.value?.toFixed(2);
                },
            },
            series: [
                {
                    type: 'map',
                    name: "some data stuff",
                    dataLabels: {
                        enabled: true,
                        color: "#FFFFFF",
                        format: "{point.postal-code}",
                        style: {
                            textTransform: "uppercase",
                        },
                    },
                    cursor: "pointer",
                    joinBy: "postal-code",
                    data: [],
                },
            ],
        };

        this.bubbleOpt_prepare = {
            title: {
                text: "Some Locations",
            },
            chart: {
                map: undefined, // will be filled later
            },
            mapNavigation: {
                enabled: true,
                enableButtons: false,
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}',
            },
            series: [
                {
                    type: 'map',
                    // Use the gb-all map with no data as a basemap
                    name: 'Basemap',
                    borderColor: '#A0A0A0',
                    nullColor: 'rgba(200, 200, 200, 0.3)',
                    color: 'rgba(200, 200, 200, 0.3)',
                    // showInLegend: false,
                }, {
                    name: 'Separators',
                    type: 'mapline',
                    nullColor: '#707070',
                    showInLegend: false,
                    enableMouseTracking: false,
                },
            ],
        };

        this.polyOpt_prepare = {
            title: {
                text: "Some Polygons",
            },
            chart: {
                map: undefined, // will be filled later
            },
            mapNavigation: {
                enabled: true,
                enableButtons: false,
            },
            series: [
                {
                    type: 'map',
                    // Use the gb-all map with no data as a basemap
                    name: 'Basemap',
                    borderColor: '#A0A0A0',
                    nullColor: 'rgba(200, 200, 200, 0.3)',
                    color: 'rgba(200, 200, 200, 0.3)',
                    showInLegend: false,
                }, {
                    name: 'Separators',
                    type: 'mapline',
                    nullColor: '#707070',
                    showInLegend: false,
                    enableMouseTracking: false,
                },
            ],
        };

        // get the world map data
        this.usMap.getWorld().then(r => {
            this.setState({usData: r.data}, () => {
                // @ts-ignore
                this.heatOpt_prepare.series![0].data = []; //make sure data is empty before  fill
                this.heatOpt_prepare["chart"]!["map"] = this.state.usData; // set the map data of the graph (using the world graph)
                // filling up some dummy data with values 1 and 2
                // instead of using the google sheet
                for (let i in this.state.usData["features"]) {
                    let mapInfo = this.state.usData["features"][i];
                    if (mapInfo["id"]) {
                        var postalCode = mapInfo.properties["postal-code"];

                        var name = mapInfo["properties"]["name"];
                        var value = 100 * Math.random();
                        // @ts-ignore
                        this.heatOpt_prepare.series![0].data.push({
                            value: value,
                            name: name,
                            "postal-code": postalCode,
                        });
                    }
                }
                // updating the map options
                this.setState({
                    heatOpt: this.heatOpt_prepare,
                });
            });
        });

        this.gbMap.getWorld().then(r => {
            this.setState({gbData: r.data}, () => {
                this.bubbleOpt_prepare["chart"]!["map"] = this.state.gbData; // set the map data of the graph (using the world graph)
                this.bubbleOpt_prepare.series?.push({
                    type: 'mappoint',
                    name: "Locations",
                    color: Highcharts.getOptions().colors![0],
                    data: [{
                        name: 'London',
                        lat: 51.507222,
                        lon: -0.1275,
                    }, {
                        name: 'Birmingham',
                        lat: 52.483056,
                        lon: -1.893611,
                    }, {
                        name: 'Leeds',
                        lat: 53.799722,
                        lon: -1.549167,
                    }, {
                        name: 'Glasgow',
                        lat: 55.858,
                        lon: -4.259,
                    }, {
                        name: 'Sheffield',
                        lat: 53.383611,
                        lon: -1.466944,
                    }, {
                        name: 'Liverpool',
                        lat: 53.4,
                        lon: -3,
                    }, {
                        name: 'Bristol',
                        lat: 51.45,
                        lon: -2.583333,
                    }, {
                        name: 'Belfast',
                        lat: 54.597,
                        lon: -5.93,
                    }, {
                        name: 'Lerwick',
                        lat: 60.155,
                        lon: -1.145,
                        dataLabels: {
                            align: 'left',
                            x: 5,
                            verticalAlign: 'middle',
                        },
                    }],
                });
                // updating the map options
                this.setState({
                    bubbleOpt: this.bubbleOpt_prepare,
                });
            });
        });

        this.caMap.getWorld().then(r => {
            this.setState({caData: r.data}, () => {
                this.polyOpt_prepare["chart"]!["map"] = this.state.caData; // set the map data of the graph (using the world graph)
                let polygonA = {
                    type: 'mappolygon',
                    name: "First Area",
                    data: [{name: 'A', lat: 46, lon: -72},
                        {name: 'B', lat: 45.3, lon: -78},
                        {name: 'C', lat: 45, lon: -73}],
                };
                let polygonB = {
                    type: 'mappolygon',
                    name: "Second Area",
                    data: [{name: 'A', lat: 46, lon: -72},
                        {name: 'B', lat: 46, lon: -71},
                        {name: 'C', lat: 45.5, lon: -70},
                        {name: 'D', lat: 45, lon: -71},
                        {name: 'E', lat: 45, lon: -73}],
                };
                // @ts-ignore
                this.polyOpt_prepare.series?.push(polygonA, polygonB);
                // updating the map options
                this.setState({
                    polyOpt: this.polyOpt_prepare,
                });
            });
        });
    }

    render() {
        const loading = (
            <div style={{
                display: 'flex',
                height: '100%',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                border: '1px solid black',
            }}>
                <p>loading...</p>
            </div>
        );

        return (
            <div>
                <div style={{display: 'inline-block', width: this.props.subplotSize, height: this.props.subplotSize}}>
                    {this.state.heatOpt ? (
                        <HighchartsReact
                            highcharts={Highcharts}
                            constructorType={"mapChart"}
                            options={this.state.heatOpt}
                        />
                    ) : (
                        loading
                    )}
                </div>
                <div style={{display: 'inline-block', width: this.props.subplotSize, height: this.props.subplotSize}}>
                    {this.state.bubbleOpt ? (
                        <HighchartsReact
                            highcharts={Highcharts}
                            constructorType={"mapChart"}
                            options={this.state.bubbleOpt}
                        />
                    ) : (
                        loading
                    )}
                </div>
                <div style={{display: 'inline-block', width: this.props.subplotSize, height: this.props.subplotSize}}>
                    {this.state.polyOpt ? (
                        <HighchartsReact
                            highcharts={Highcharts}
                            constructorType={"mapChart"}
                            options={this.state.polyOpt}
                        />
                    ) : (
                        loading
                    )}
                </div>
            </div>
        );
    }
}

export default HighchartMap;
