import React from 'react';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsReact from 'highcharts-react-official';
import {createBigData} from './utils';

require('highcharts/modules/boost')(Highcharts);

HighchartsMore(Highcharts);

type HighchartBigProps = {};

type HighchartBigState = {
    options: Highcharts.Options; // this is recommended
    interpolation: number;
};

class HighchartBig extends React.Component<HighchartBigProps, HighchartBigState> {
    data = createBigData();


    constructor(props: Readonly<HighchartBigProps> | HighchartBigProps) {
        super(props);
        this.state = {
            interpolation: 0,
            options: {
                chart: {
                    zoomType: 'x',
                },
                boost: {
                    enabled: true,
                    allowForce: true,
                    seriesThreshold: 5,

                },
                title: {
                    text: 'Some Sample Plot',
                },
                subtitle: {
                    text: 'Created with Highcharts',
                },
                xAxis: {
                    type: 'datetime', // needed!
                },
                series: this.data.map(country => ({
                    boostThreshold: 1,
                    type: 'spline',
                    name: country.who,
                    data: country.values.map(entry => ([entry.when.getTime(), entry.what[0]])),
                })),
                tooltip: {
                    shared: true,
                },
            },
        };
    }

    changeBigData = (e: any) => {
        const newVal = e.target.value;
        this.setState({
            interpolation: newVal,
            options: {
                ...this.state.options,
                series: this.data.map(country => ({
                    boostThreshold: 1,
                    type: 'spline',
                    name: country.who,
                    data: country.values.map(v => ([v.when.getTime(), (100 - newVal) / 100 * v.what[0] + (newVal) / 100 * v.what[1]])),
                })),
            },
        });
    };

    render() {
        return (
            <div>
                <input type={'number'} min={0} max={100} step={5}
                       value={this.state.interpolation} onChange={this.changeBigData}/>
                <HighchartsReact highcharts={Highcharts} options={this.state.options}/>
            </div>
        );
    }
}

export default HighchartBig;
