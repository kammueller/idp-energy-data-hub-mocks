import Plot from "react-plotly.js";
import React from "react";
import {ModeBarButton} from 'plotly.js';
import tumLogo from './tum.png';
import {calculateConf, createBigData} from './utils';
import PlotlyBig from './PlotlyBig';

type PlotlyProps = {};

type PlotlyState = {
    yValues: number[];
    yConf: number[];
};

export default class Plotly extends React.Component<PlotlyProps, PlotlyState> {
    bigData = createBigData();

    constructor(props: Readonly<PlotlyProps> | PlotlyProps) {
        super(props);
        let values = [1, 3, 2, 3, 1.5];
        this.state = {
            yValues: values,
            yConf: Plotly.calculateConfPolygon(values),
        };
    }

    /**
     * calculate some deviance for the last two entries to fake a conf interval
     */
    static calculateConfPolygon(values: number[]): number[] {
        if (values.length < 3) {
            return [];
        }

        let bounds = calculateConf(values);

        let array = [];
        for (let i = bounds.length - 1; i >= 0; i--) {
            array.push(bounds[i][0]);
        }
        for (let i = 0; i <= bounds.length - 1; i++) {
            array.push(bounds[i][1]);
        }

        return array;
    }

    changeScatter = () => {
        let values = [1, 3, 2, 2 + 2 * Math.random(), 1 + Math.random()];
        this.setState({
            yValues: values,
            yConf: Plotly.calculateConfPolygon(values),
        });
    };

    render() {
        let icon1 = {
            'width': 500,
            'height': 600,
            'path': 'M224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64zm215.39-149.71c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71z',
        };

        let button: ModeBarButton = {
            name: 'button1',
            title: 'button1',
            icon: icon1,
            gravity: 'up',
            click: this.changeScatter,
        };
        return (
            <div>
                <h3>Basic Charts</h3>
                <button onClick={this.changeScatter}>Randomize Interval</button>
                (you can also use the bell button top right in the toolbar)
                <Plot
                    style={{width: '100%', height: 400}} // needed for responsive
                    data={[
                        {
                            x: [1, 2, 3, 4, 5],
                            y: this.state.yValues,
                            mode: 'lines',
                            line: {
                                dash: 'dashdot',
                                shape: 'spline',
                            },
                            type: 'scatter',
                            name: 'scatter',
                            legendgroup: 's',
                        },
                        {
                            // move a polygon all the way around! use "beginning twice"
                            x: [5, 4, 3, 2, 1, 1, 2, 3, 4, 5],
                            y: this.state.yConf,
                            fill: "toself", // use this for solid cut at the end
                            fillcolor: "rgba(0,176,246,0.2)",
                            line: {
                                color: "transparent",
                                shape: "spline",
                                smoothing: 1,
                            },
                            hoverinfo: 'skip', // not none, since this would do weird stuff with the other tooltips
                            type: "scatter",
                            name: "Scatter conf.int.",
                            legendgroup: 's',
                        },
                        {
                            x: [0.5, 0.9, 1.33, 2, 3, 7.2],
                            y: [0, 2, 1.33, 6, 3, 7.2],
                            type: 'scatter',
                            mode: 'lines+markers',
                            marker: {color: 'red'},
                            line: {
                                color: 'orange',
                                shape: 'spline',
                            },
                            name: 'Interpolated scatter',
                        },
                        {
                            x: [0.5, 0.9, 1.33, 2, 3, 6],
                            y: [10, 15, 10.33, 17, 12, 20],
                            type: 'scatter',
                            mode: 'lines+markers',
                            yaxis: 'y2',
                            marker: {color: 'orange'},
                            line: {
                                color: 'red',
                            },
                            name: 'Other axis',
                        },
                        /* {
                             x: [0.5, 0.9, 1.33, 2, 3, 6],
                             y: [1, 15, 10.33, 17, 12, 20],
                             type: 'scatter',
                             mode: 'lines+markers',
                             yaxis: 'y3',
                             name: 'Logarithmic',
                         },*/
                    ]}
                    layout={{
                        title: 'A Fancy Plot',
                        font: {size: 18},
                        legend: {
                            orientation: 'h',
                            y: -0.2, // TODO: meh
                        },
                        yaxis: {automargin: true}, // definitely set that!
                        margin: {pad: 0},

                        yaxis2: {
                            title: 'yaxis2 title',
                            titlefont: {color: 'rgb(148, 103, 189)'},
                            tickfont: {color: 'rgb(148, 103, 189)'},
                            overlaying: 'y',
                            side: 'right',
                            showgrid: false,
                        },
                        /*yaxis3: {
                            title: {
                                text: 'yaxis3 title',
                                standoff: 0,
                                font:  {color: 'rgb(116,189,103)'}
                            },
                            tickfont: {color: 'rgb(116,189,103)'},
                            ticks: 'inside',
                            type: 'log',
                            range: [0, 1.5], // log scale!!
                            overlaying: 'y',
                            side: 'right',
                            gridcolor: 'rgb(116,189,103)',
                        },*/

                        images: [
                            {
                                x: 0,
                                y: 0.8,
                                sizex: 0.2,
                                sizey: 0.2,
                                source: tumLogo,
                                xanchor: "left",
                                xref: "paper",
                                yanchor: "bottom",
                                yref: "paper",
                            },
                        ],
                    }}
                    // useful: staticPlot: true
                    config={{
                        scrollZoom: true,
                        displaylogo: false,
                        toImageButtonOptions: {
                            format: 'svg',
                            scale: 2,
                        },
                        modeBarButtonsToAdd: [button],
                        responsive: true,
                    }}
                />

                <Plot
                    data={[{
                        type: 'pie',
                        values: [55, 26, 3],
                        labels: ['foo', 'bar', 'quux'],
                    }]}
                    layout={{
                        height: 400,
                        width: 500,
                        title: 'Used Variable Names in this Code',
                    }}
                />
                <Plot
                    data={[
                        {
                            type: 'bar',
                            name: 'Regular Code',
                            y: [55, 26, 3],
                            x: ['foo', 'bar', 'quux'],
                        }, {
                            type: 'bar',
                            name: 'Weird Code',
                            y: [23, 3, 99],
                            x: ['foo', 'bar', 'quux'],
                            error_y: {
                                type: 'data',
                                symmetric: false,
                                array: [3, 5, 3],
                                arrayminus: [3, 1, 20],
                                color: '#888',
                            },
                        },
                    ]}
                    layout={{
                        height: 400,
                        width: 500,
                        title: 'Used Variable Names',
                    }}
                />


                <h3>Correlated Charts</h3>
                <Plot
                    data={[
                        {
                            x: [0, 1, 1, 0, 0, 1, 1, 2, 2, 3, 3, 2, 2, 3],
                            y: [0, 0, 1, 1, 3, 3, 2, 2, 3, 3, 1, 1, 0, 0],
                        },
                        {
                            x: [0, 1, 2, 3],
                            y: [1, 2, 4, 8],
                            yaxis: "y2",
                        },
                        {
                            x: [1, 10, 100, 10, 1],
                            y: [0, 1, 2, 3, 4],
                            xaxis: "x2",
                            yaxis: "y3",
                        },
                        {
                            x: [1, 100, 30, 80, 1],
                            y: [1, 1.5, 2, 2.5, 3],
                            xaxis: "x2",
                            yaxis: "y4",
                        },
                    ]}
                    layout={{
                        height: 400,
                        width: 800,
                        title: 'Fixed-Ratio Axes',
                        xaxis: {
                            nticks: 10,
                            domain: [0, 0.45],
                            title: "shared X axis",
                        },
                        yaxis: {
                            scaleanchor: "x", // scales together with this axis
                            domain: [0, 0.45],
                            title: "1:1",
                        },
                        yaxis2: {
                            scaleanchor: "x",
                            scaleratio: 0.2,
                            domain: [0.55, 1],
                            title: "1:5",
                        },
                        xaxis2: {
                            type: "log",
                            domain: [0.55, 1],
                            anchor: "y3",
                            title: "unconstrained log X",
                        },
                        yaxis3: {
                            domain: [0, 0.45],
                            anchor: "x2",
                            title: "matches ->",
                        },
                        yaxis4: {
                            scaleanchor: "y3",
                            domain: [0.55, 1],
                            anchor: "x2",
                            title: "<- matches",
                        },
                    }}
                />
                <Plot
                    data={[
                        {
                            x: [1, 2, 3],
                            y: [2, 3, 4],
                            type: 'scatter',
                            name: 'Data Set 1',
                        }, {
                            x: [2, 3, 4],
                            y: [600, 700, 800],
                            xaxis: 'x',
                            yaxis: 'y3',
                            type: 'scatter',
                            name: 'Data Set 2',
                        },
                    ]}
                    layout={{
                        height: 400,
                        width: 400,
                        title: 'Same x axis, stacked y axes',
                        grid: {
                            rows: 2,
                            columns: 1,
                            subplots: [['xy'], ['xy3']], // TODO need to adopt the type definition for this...
                            roworder: 'bottom to top',
                        },
                        spikedistance: -1, // TODO: here as well
                        xaxis: {
                            showspikes: true,
                            spikemode: 'across',
                            spikesnap: 'cursor',
                            spikecolor: '#999',
                            spikethickness: 1,
                            spikedash: 'solid',
                        },
                        showlegend: false,
                    }}
                    config={{
                        displayModeBar: false,
                        // showAxisDragHandles: true,
                    }}
                />


                <h3>Maps</h3>
                <Plot
                    data={[{
                        type: 'scattermapbox',
                        text: ['a', 'b', 'c'],
                        lon: [-91, -90, -89],
                        lat: [39, 38, 37],
                        marker: {color: 'fuchsia', size: 15},
                    }]}
                    layout={{
                        height: 400,
                        width: 400,
                        dragmode: 'zoom',
                        mapbox: {
                            style: 'open-street-map', // or 'stamen-toner',
                            center: {lat: 38, lon: -90},
                            zoom: 6,
                        },
                        margin: {r: 0, t: 0, b: 0, l: 0},
                    }}
                />
                <Plot
                    data={[{
                        type: 'choropleth',
                        locationmode: 'USA-states',
                        locations: ['AL', 'FL', 'ME'],
                        z: [30, 50, 90],
                    }]}
                    layout={{
                        height: 400,
                        width: 400,
                        title: '2011 US Agriculture Exports by State',
                        geo: {
                            scope: 'usa',
                            showlakes: true,
                            lakecolor: 'rgb(255,255,255)',
                        },
                    }}
                />
                <Plot
                    data={[
                        {
                            type: 'scattermapbox',
                            fill: 'toself',
                            lon: [-72, -71, -70, -71, -73],
                            lat: [46, 46, 45.5, 45, 45],
                            marker: {size: 0, color: "orange"},
                            name: 'Some Area',
                        }, {
                            type: 'scattermapbox',
                            fill: 'toself',
                            lon: [-72, -78, -73],
                            lat: [46, 45.3, 45],
                            marker: {size: 0, color: "red"},
                            name: 'Another Area',
                        },
                    ]}
                    layout={{
                        height: 400,
                        width: 400,
                        mapbox: {
                            style: "stamen-terrain",
                            center: {lon: -73, lat: 46},
                            zoom: 5,
                        },
                        showlegend: true,
                        legend: {orientation: 'h'},
                        margin: {r: 0, t: 0, b: 0, l: 0},
                    }}
                />

                <h3>Mass Test</h3>
                <PlotlyBig data={this.bigData}/>
            </div>
        );
    }
}
