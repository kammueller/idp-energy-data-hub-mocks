/**
 * calculate some deviance for the last two entries to fake a conf interval
 */
export function calculateConf(values: number[]): Array<number[]> {
    if (values.length < 3) {
        return [];
    }

    let last = values[values.length - 1];
    let l_lower = (0.5 * Math.random()) * last;
    let l_upper = (1.5 + 0.5 * Math.random()) * last;
    let prior = values[values.length - 2];
    let p_lower = (0.5 + 0.4 * Math.random()) * prior;
    let p_upper = (1.1 + 0.4 * Math.random()) * prior;

    let array = [];
    for (let i = 0; i <= values.length - 3; i++) {
        array.push([values[i], values[i]]);
    }
    array.push([p_lower, p_upper]);
    array.push([l_lower, l_upper]);

    return array;
}

export type BigSeriesEntry = {
    when: Date;
    what: Array<number>;
}

export type BigSeriesCountry = {
    who: string;
    values: Array<BigSeriesEntry>;
}

/**
 * creates a dataset for 12 countries, 600 time slots, 2 versions
 */
export function createBigData(): Array<BigSeriesCountry> {

    let series: BigSeriesCountry[] = [];
    for (let country = 0; country < 12; country++) {
        let entries: BigSeriesEntry[] = [];

        let date = new Date("2000-01-01");
        for (let time = 0; time < 600; time++) {
            let values: number[] = [10 * country + 0.5 * time * Math.random(), country + 1.5 * time * Math.random()];
            entries.push({when: new Date(date), what: values});
            date.setDate(date.getDate() + 1);
        }

        series.push({who: country.toString(), values: entries});
    }

    return series;
}