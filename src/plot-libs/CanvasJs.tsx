import React from "react";
import CanvasJSReact from '../libs/canvasjs/canvasjs.react';
import {createBigData} from './utils';

let CanvasJS = CanvasJSReact.CanvasJS;
let CanvasJSChart = CanvasJSReact.CanvasJSChart;

type CanvasJsProps = {};

type CanvasJsState = {};

class CanvasJs extends React.Component<CanvasJsProps, CanvasJsState> {
    private chart: any;
    private bigChart: any;
    private bigData = createBigData();

    alterData = () => {
        let spline = this.chart.options.data[1].dataPoints;
        let interval = this.chart.options.data[0].dataPoints;
        for (let i = 1; i < 3; ++i) {
            let rnd = 0.75 + 0.5 * Math.random();
            spline[spline.length - i].y *= rnd;
            interval[interval.length - i].y[0] *= rnd;
            interval[interval.length - i].y[1] *= rnd;
        }
        this.chart.options.title.text = 'foo';
        this.chart.render();
    };

    changeBigData = (e: any) => {
        const newVal = e.target.value;
        let countries = this.bigChart.options.data;
        console.log(countries);
        for (let i = 0; i < countries.length; ++i) {
            let country = countries[i];
            for (let j = 0; j < country.dataPoints.length; ++j) {
                let dataPoint = country.dataPoints[j];
                let v = this.bigData[i].values[j];
                dataPoint.y = (100 - newVal) / 100 * v.what[0] + (newVal) / 100 * v.what[1];
            }
        }
        this.bigChart.render();
    };

    render() {
        const subFigureWidth = 500;

        const optOne = {
            zoomEnabled: true,
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Basic Chart in React",
            },
            data: [
                {
                    type: "rangeSplineArea",
                    name: "First Quarter -- 95%",
                    color: 'rgba(0,176,246,0.2)',
                    showInLegend: true,
                    markerType: 'none',
                    dataPoints: [
                        {x: 20, y: [15, 15]},
                        {x: 30, y: [25, 25]},
                        {x: 40, y: [25, 35]},
                        {x: 50, y: [20, 40]},
                    ],
                }, {
                    type: "spline",
                    name: "First Quarter",
                    color: 'rgba(0,176,246,1)',
                    showInLegend: true,
                    dataPoints: [
                        {x: 10, y: 10},
                        {x: 20, y: 15},
                        {x: 30, y: 25},
                        {x: 40, y: 30},
                        {x: 50, y: 28},
                    ],
                }, {
                    type: "line",
                    name: "Emissions",
                    axisYType: "secondary",
                    lineDashType: "dash",
                    showInLegend: true,
                    dataPoints: [
                        {x: 10, y: 56},
                        {x: 20, y: 57},
                        {x: 30, y: 80},
                        {x: 40, y: 70},
                        {x: 50, y: 98},
                    ],
                },
            ],
            axisX: {
                crosshair: {
                    enabled: true,
                    // snapToDataPoint: true,
                },
            },
            axisY: {
                prefix: "$",
                suffix: "K",
            },
            axisY2: {
                suffix: 't',
                title: 'CO2 Emission',
                // logarithmic: true,
                // reversed: true,
            },
            legend: {
                itemclick: function (e: any) {
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }
                    e.chart.render();
                },
            },
        };

        const optPie = {
            width: subFigureWidth,
            height: 300,
            title: {
                text: "A Pie Chart",
            },
            data: [
                {
                    type: "pie", //change type to bar, line, area, pie, etc
                    name: "Emissions",
                    dataPoints: [
                        {label: 'A', y: 71},
                        {label: 'B', y: 55},
                        {label: 'C', y: 50},
                        {label: 'D', y: 65},
                        {label: 'E', y: 95},
                        {label: 'F', y: 68},
                        {label: 'G', y: 28},
                        {label: 'H', y: 34},
                        {label: 'I', y: 14},
                    ],
                },
            ],
        };

        const optBar = {
            width: subFigureWidth,
            height: 300,
            title: {
                text: "A Bar Chart",
            },
            toolTip: {
                shared: true,
            },
            data: [
                {
                    type: "column",
                    name: "first property",
                    showInLegend: true,
                    dataPoints: [
                        {y: 198, label: "Italy"},
                        {y: 201, label: "China"},
                        {y: 202, label: "France"},
                        {y: 236, label: "Great Britain"},
                        {y: 395, label: "Soviet Union"},
                        {y: 957, label: "USA"},
                    ],
                },
                {
                    type: "column",
                    name: "second property",
                    showInLegend: true,
                    dataPoints: [
                        {y: 166, label: "Italy"},
                        {y: 144, label: "China"},
                        {y: 223, label: "France"},
                        {y: 272, label: "Great Britain"},
                        {y: 319, label: "Soviet Union"},
                        {y: 759, label: "USA"},
                    ],
                },
                {
                    type: "column",
                    name: "third property",
                    showInLegend: true,
                    dataPoints: [
                        {y: 185, label: "Italy"},
                        {y: 128, label: "China"},
                        {y: 246, label: "France"},
                        {y: 272, label: "Great Britain"},
                        {y: 296, label: "Soviet Union"},
                        {y: 666, label: "USA"},
                    ],
                },
                {
                    type: "error",
                    name: "Variability Range",
                    toolTipContent: "<span style=\"color:#C0504E\">{name}</span>: {y[0]} - {y[1]}",
                    dataPoints: [
                        {y: [185, 195], label: "Italy"},
                        {y: [118, 158], label: "China"},
                        {y: [226, 256], label: "France"},
                        {y: [232, 282], label: "Great Britain"},
                        {y: [286, 296], label: "Soviet Union"},
                        {y: [606, 686], label: "USA"},
                    ],
                },
            ],
        };

        const optBox = {
            width: subFigureWidth,
            height: 300,
            title: {
                text: "A Box Plot",
            },
            data: [{
                type: "boxAndWhisker",
                // yValueFormatString: "#,##0.# \"kcal/100g\"",
                dataPoints: [
                    {label: "Bread", y: [179, 256, 300, 418, 274]},
                    {label: "Cake", y: [252, 346, 409, 437, 374.5]},
                    {label: "Biscuit", y: [236, 281.5, 336.5, 428, 313]},
                    {label: "Doughnut", y: [340, 382, 430, 452, 417]},
                    {label: "Pancakes", y: [194, 224.5, 342, 384, 251]},
                    {label: "Bagels", y: [241, 255, 276.5, 294, 274.5]},
                ],
            }],
        };

        const optBig = {
            zoomEnabled: true,
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "A CanvasJS Mass Test",
            },
            data: this.bigData.map(country => ({
                type: "spline",
                name: country.who,
                showInLegend: true,
                dataPoints: country.values.map(val => ({x: val.when, y: val.what[0]})),
            })),
            legend: {
                itemclick: function (e: any) {
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }
                    e.chart.render();
                },
            },
            toolTip: {
                shared: true,
            },
        };

        return (
            <div>
                <h3>Basic Charts</h3>
                <button onClick={this.alterData}>Change Data</button>
                <CanvasJSChart options={optOne} onRef={(ref: any) => (this.chart = ref)}/>
                <div style={{width: subFigureWidth, height: 300, display: 'inline-block'}}>
                    <CanvasJSChart options={optPie}/>
                </div>
                <div style={{width: subFigureWidth, height: 300, display: 'inline-block'}}>
                    <CanvasJSChart options={optBar}/>
                </div>
                <div style={{width: subFigureWidth, height: 300, display: 'inline-block'}}>
                    <CanvasJSChart options={optBox}/>
                </div>

                <h3>Correlated Charts</h3>
                {/* TODO ? */}
                <a href="https://canvasjs.com/javascript-charts/sync-multiple-chart-tooltip/">Click here</a>

                <h3>Mass Test</h3>
                <input type={'number'} min={0} max={100} defaultValue={0} step={5} onChange={this.changeBigData}/>
                <CanvasJSChart options={optBig} onRef={(ref: any) => (this.bigChart = ref)}/>
            </div>
        );
    }
}

export default CanvasJs;
