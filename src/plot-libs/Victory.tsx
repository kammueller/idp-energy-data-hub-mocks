import React from "react";
import {
    VictoryAxis,
    VictoryBrushContainer,
    VictoryChart, VictoryLegend,
    VictoryLine,
    VictoryStack,
    VictoryTheme,
    VictoryZoomContainer,
} from 'victory';
import {DomainTuple} from 'victory-core';

type VictoryProps = {};

type ZoomDomain = {
    x?: DomainTuple;
    y?: DomainTuple;
};

type VictoryState = {
    zoomDomain1: ZoomDomain;
};

class Victory extends React.Component<VictoryProps, VictoryState> {
    constructor(props: Readonly<VictoryProps> | VictoryProps) {
        super(props);
        this.state = {
            zoomDomain1: {x: [new Date(1990, 1, 1), new Date(2009, 1, 1)]},
        };
    }

    handleZoom1 = (domain: ZoomDomain) => {
        this.setState({zoomDomain1: domain});
    };

    render() {
        const data0 = [
            {a: new Date(1982, 1, 1), b: 125},
            {a: new Date(1987, 1, 1), b: 257},
            {a: new Date(1993, 1, 1), b: 345},
            {a: new Date(1997, 1, 1), b: 515},
            {a: new Date(2001, 1, 1), b: 132},
            {a: new Date(2005, 1, 1), b: 305},
            {a: new Date(2011, 1, 1), b: 270},
            {a: new Date(2015, 1, 1), b: 470},
        ];
        const data1 = [
            {a: new Date(1982, 1, 1), b: 1125},
            {a: new Date(1987, 1, 1), b: 1257},
            {a: new Date(1993, 1, 1), b: 1345},
            {a: new Date(1997, 1, 1), b: 2515},
            {a: new Date(2001, 1, 1), b: 2132},
            {a: new Date(2005, 1, 1), b: 2305},
            {a: new Date(2011, 1, 1), b: 3270},
            {a: new Date(2015, 1, 1), b: 3470},
        ];

        let lines = <VictoryStack colorScale={'warm'}>
            <VictoryLine data={data0} x={'a'} y={'b'} style={{data: {strokeDasharray: "5,5"}}}/>
            <VictoryLine data={data1} x={'a'} y={'b'} interpolation={'monotoneX'}/>
        </VictoryStack>;

        return (
            <div>
                <h3>Basic Charts</h3>
                <div>
                    <VictoryChart
                        height={300} width={800}
                        domainPadding={20} theme={VictoryTheme.material}
                        containerComponent={
                            <VictoryZoomContainer
                                zoomDimension="x"
                                zoomDomain={this.state.zoomDomain1}
                                onZoomDomainChange={this.handleZoom1}
                            />
                        }
                        scale={{x: "time"}}
                    >
                        <VictoryAxis/>
                        <VictoryAxis dependentAxis={true}/>
                        {lines}

                        <VictoryLegend/>
                    </VictoryChart>
                    <VictoryChart
                        height={75} width={800}
                        padding={{top: 0, left: 50, right: 50, bottom: 30}}
                        scale={{x: "time"}}
                        containerComponent={
                            <VictoryBrushContainer
                                brushDimension="x"
                                brushDomain={this.state.zoomDomain1}
                                onBrushDomainChange={this.handleZoom1}
                            />
                        }
                    >
                        <VictoryAxis tickFormat={(x) => new Date(x).getFullYear()}/>
                        {lines}
                    </VictoryChart>
                </div>
            </div>
        );
    }
}

export default Victory;
