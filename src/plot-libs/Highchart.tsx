import React from 'react';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsReact from 'highcharts-react-official';
import './Highchart.css';
import {calculateConf} from './utils';
import HighchartBig from './HighchartBig';
import HighchartMap from './HighchartMap';

require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);
require('highcharts/modules/offline-exporting')(Highcharts); // needed to not submit data!
require('highcharts/modules/sankey')(Highcharts);
require('highcharts/modules/dependency-wheel')(Highcharts);
require('highcharts/modules/networkgraph')(Highcharts);

HighchartsMore(Highcharts);

type HighchartProps = {};

type HighchartState = {
    yValues: number[];
    yConf: Array<number[]>;
};

class Highchart extends React.Component<HighchartProps, HighchartState> {
    // TODO: setup own export stuff (https://www.highcharts.com/docs/getting-started/frequently-asked-questions)


    constructor(props: Readonly<HighchartProps> | HighchartProps) {
        super(props);
        let values = [1, 3, 2, 3, 1.5];
        this.state = {
            yValues: values,
            yConf: calculateConf(values),
        };
    }

    changeInterval = () => {
        let values = [1, 3, 2, 2 + 2 * Math.random(), 1 + Math.random()];
        this.setState({yValues: values, yConf: calculateConf(values)});
    };

    render() {
        const subplotWidth = 600;

        const optionsLine: Highcharts.Options = {
            chart: {
                zoomType: 'x',
                panKey: 'shift', // how does this work?
            },
            title: {
                text: 'Some Sample Plot',
            },
            subtitle: {
                text: 'Created with Highcharts',
            },
            yAxis: [
                {
                    type: 'linear',
                    min: 12,
                    max: 20,
                    tickPixelInterval: 100,
                    title: {
                        text: 'Black axis',
                    },
                    labels: {
                        formatter: function () {
                            return this.value + 'k US$';
                        },
                    },
                }, {
                    max: 6,
                    title: {
                        text: 'Blue axis',
                        style: {color: Highcharts.getOptions().colors![0]},
                    },
                    gridLineColor: Highcharts.getOptions().colors![0],
                },
            ],
            series: [
                {
                    type: 'spline', // set this to fix the type!
                    name: 'Interpolated Data',
                    data: this.state.yValues,
                    yAxis: 1,
                },
                {
                    type: 'areasplinerange',
                    name: 'Confidence Interval',
                    data: this.state.yConf,
                    color: Highcharts.getOptions().colors![0],
                    lineWidth: 0,
                    fillOpacity: 0.3,
                    marker: {
                        enabled: false,
                    },
                    linkedTo: ':previous',
                    yAxis: 1,
                }, {
                    type: 'line',
                    name: 'Linear Data',
                    data: [[0, 15], [1.5, 17], [2, 20], [4, 13]],
                    dashStyle: 'LongDash',
                },
            ],
            tooltip: {
                shared: true,
            },
        };

        const optionsBar: Highcharts.Options = {
            chart: {
                width: subplotWidth,
            },
            title: {
                text: 'A Bar Plot',
            },
            xAxis: {
                categories: ['foo', 'bar', 'quux'],
                title: {
                    text: 'Used Variable Names',
                },
            },
            series: [{
                type: 'column',
                name: 'Regular Code',
                data: [55, 26, 3],
            }, {
                type: 'errorbar',
                name: 'Regular Code Error',
                data: [[55, 55], [24, 27], [2.5, 7]],
                tooltip: {
                    pointFormat: '(error range: {point.low}-{point.high})<br/>',
                },
            }, {
                type: 'column',
                name: 'Weird Code',
                data: [23, 3, 99],
            }, {
                type: 'errorbar',
                name: 'Weird Code Error',
                data: [[20, 26], [2, 8], [79, 102]],
                tooltip: {
                    pointFormat: '(error range: {point.low}-{point.high})<br/>',
                },
            }, {
                type: 'column',
                name: 'No Code ever',
                data: [0, 0, 0],
            }],
            tooltip: {
                shared: true,
            },
        };

        const optionsBox: Highcharts.Options = {
            chart: {
                width: subplotWidth,
            },
            title: {
                text: 'A Box Plot',
            },
            series: [{
                type: 'boxplot',
                name: 'Observations',
                data: [
                    [760, 801, 848, 895, 965],
                    [733, 853, 939, 980, 1080],
                    [714, 762, 817, 870, 918],
                    [724, 802, 806, 871, 950],
                    [834, 836, 864, 882, 910],
                ],
            }, {
                type: 'scatter',
                name: 'Outliers',
                color: Highcharts.getOptions().colors![0],
                data: [ // x, y positions where 0 is the first category
                    [0, 644],
                    [4, 718],
                    [4, 951],
                    [4, 969],
                ],
                marker: {
                    fillColor: 'white',
                    lineWidth: 1,
                    lineColor: Highcharts.getOptions().colors![0],
                },
                tooltip: {
                    pointFormat: 'Observation: {point.y}',
                },
            }],

        };

        const optionsPie: Highcharts.Options = {
            chart: {
                width: subplotWidth,
            },
            title: {
                text: 'A Pie Plot',
            },
            series: [{
                type: 'pie',
                name: 'Variable Names',
                data: [
                    {
                        name: 'Foo',
                        y: 55,
                    }, {
                        name: 'Bar',
                        y: 26,
                    }, {
                        name: 'Quux',
                        y: 3,
                    },
                ],
            }],
            tooltip: {
                shared: true,
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y:.0f}',
                    },
                },
            },
        };

        const optionsSankey: Highcharts.Options = {
            chart: {
                width: subplotWidth,
            },
            title: {
                text: 'Highcharts Sankey Diagram',
            },
            series: [{
                type: 'sankey',
                name: 'Sankey demo series',
                data: [
                    {from: 'Brazil', to: 'Portugal', weight: 5},
                    {from: 'Brazil', to: 'France', weight: 1},
                    {from: 'Brazil', to: 'Spain', weight: 1},
                    {from: 'Brazil', to: 'England', weight: 1},
                    {from: 'Canada', to: 'Portugal', weight: 1},
                    {from: 'Canada', to: 'France', weight: 5},
                    {from: 'Canada', to: 'England', weight: 1},
                    {from: 'Mexico', to: 'Portugal', weight: 1},
                    {from: 'Mexico', to: 'France', weight: 1},
                    {from: 'Mexico', to: 'Spain', weight: 5},
                    {from: 'Mexico', to: 'England', weight: 1},
                    {from: 'USA', to: 'Portugal', weight: 1},
                    {from: 'USA', to: 'France', weight: 1},
                    {from: 'USA', to: 'Spain', weight: 1},
                    {from: 'USA', to: 'England', weight: 5},
                    {from: 'Portugal', to: 'Angola', weight: 2},
                    {from: 'Portugal', to: 'Senegal', weight: 1},
                    {from: 'Portugal', to: 'Morocco', weight: 1},
                    {from: 'Portugal', to: 'South Africa', weight: 3},
                    {from: 'France', to: 'Angola', weight: 1},
                    {from: 'France', to: 'Senegal', weight: 3},
                    {from: 'France', to: 'Mali', weight: 3},
                    {from: 'France', to: 'Morocco', weight: 3},
                    {from: 'France', to: 'South Africa', weight: 1},
                    {from: 'Spain', to: 'Senegal', weight: 1},
                    {from: 'Spain', to: 'Morocco', weight: 3},
                    {from: 'Spain', to: 'South Africa', weight: 1},
                    {from: 'England', to: 'Angola', weight: 1},
                    {from: 'England', to: 'Senegal', weight: 1},
                    {from: 'England', to: 'Morocco', weight: 2},
                    {from: 'England', to: 'South Africa', weight: 7},
                    {from: 'South Africa', to: 'China', weight: 5},
                    {from: 'South Africa', to: 'India', weight: 1},
                    {from: 'South Africa', to: 'Japan', weight: 3},
                    {from: 'Angola', to: 'China', weight: 5},
                    {from: 'Angola', to: 'India', weight: 1},
                    {from: 'Angola', to: 'Japan', weight: 3},
                    {from: 'Senegal', to: 'China', weight: 5},
                    {from: 'Senegal', to: 'India', weight: 1},
                    {from: 'Senegal', to: 'Japan', weight: 3},
                    {from: 'Mali', to: 'China', weight: 5},
                    {from: 'Mali', to: 'India', weight: 1},
                    {from: 'Mali', to: 'Japan', weight: 3},
                    {from: 'Morocco', to: 'China', weight: 5},
                    {from: 'Morocco', to: 'India', weight: 1},
                    {from: 'Morocco', to: 'Japan', weight: 3},
                ],
            }],
        };

        const optionsChord: Highcharts.Options = {
            chart: {
                width: subplotWidth,
            },
            title: {
                text: 'Highcharts Chord Diagram',
            },
            series: [{
                type: 'dependencywheel',
                name: 'Dependency wheel series',
                dataLabels: {
                    color: '#333',
                    textPath: {
                        enabled: true,
                        attributes: {
                            dy: 5,
                        },
                    },
                    // @ts-ignore
                    distance: 10,
                },
                data: [
                    {from: 'Brazil', to: 'Portugal', weight: 5},
                    {from: 'Brazil', to: 'France', weight: 1},
                    {from: 'Brazil', to: 'Spain', weight: 1},
                    {from: 'Brazil', to: 'England', weight: 1},
                    {from: 'Canada', to: 'Portugal', weight: 1},
                    {from: 'Canada', to: 'France', weight: 5},
                    {from: 'Canada', to: 'England', weight: 1},
                    {from: 'Mexico', to: 'Portugal', weight: 1},
                    {from: 'Mexico', to: 'France', weight: 1},
                    {from: 'Mexico', to: 'Spain', weight: 5},
                    {from: 'Mexico', to: 'England', weight: 1},
                    {from: 'USA', to: 'Portugal', weight: 1},
                    {from: 'USA', to: 'France', weight: 1},
                    {from: 'USA', to: 'Spain', weight: 1},
                    {from: 'USA', to: 'England', weight: 5},
                    {from: 'Portugal', to: 'Angola', weight: 2},
                    {from: 'Portugal', to: 'Senegal', weight: 1},
                    {from: 'Portugal', to: 'Morocco', weight: 1},
                    {from: 'Portugal', to: 'South Africa', weight: 3},
                    {from: 'France', to: 'Angola', weight: 1},
                    {from: 'France', to: 'Senegal', weight: 3},
                    {from: 'France', to: 'Mali', weight: 3},
                    {from: 'France', to: 'Morocco', weight: 3},
                    {from: 'France', to: 'South Africa', weight: 1},
                    {from: 'Spain', to: 'Senegal', weight: 1},
                    {from: 'Spain', to: 'Morocco', weight: 3},
                    {from: 'Spain', to: 'South Africa', weight: 1},
                    {from: 'England', to: 'Angola', weight: 1},
                    {from: 'England', to: 'Senegal', weight: 1},
                    {from: 'England', to: 'Morocco', weight: 2},
                    {from: 'England', to: 'South Africa', weight: 7},
                    {from: 'South Africa', to: 'China', weight: 5},
                    {from: 'South Africa', to: 'India', weight: 1},
                    {from: 'South Africa', to: 'Japan', weight: 3},
                    {from: 'Angola', to: 'China', weight: 5},
                    {from: 'Angola', to: 'India', weight: 1},
                    {from: 'Angola', to: 'Japan', weight: 3},
                    {from: 'Senegal', to: 'China', weight: 5},
                    {from: 'Senegal', to: 'India', weight: 1},
                    {from: 'Senegal', to: 'Japan', weight: 3},
                    {from: 'Mali', to: 'China', weight: 5},
                    {from: 'Mali', to: 'India', weight: 1},
                    {from: 'Mali', to: 'Japan', weight: 3},
                    {from: 'Morocco', to: 'China', weight: 5},
                    {from: 'Morocco', to: 'India', weight: 1},
                    {from: 'Morocco', to: 'Japan', weight: 3},
                ],
            }],
        };

        const optionsNetwork: Highcharts.Options = {
            chart: {
                width: subplotWidth,
            },
            title: {
                text: 'Highcharts Network Diagram',
            },
            plotOptions: {
                networkgraph: {
                    keys: ['from', 'to'],
                    layoutAlgorithm: {
                        enableSimulation: true,
                        friction: -0.9,
                    },
                },
            },
            series: [{
                type: 'networkgraph',
                dataLabels: {
                    enabled: true,
                    linkFormat: '',
                },
                id: 'lang-tree',
                data: [
                    {from: 'Root', to: 'Node A'},
                    {from: 'Root', to: 'Node B'},
                    {from: 'Root', to: 'Node C'},
                    {from: 'Node A', to: 'Node B'},
                    {from: 'Node C', to: 'Node B'},
                    {from: 'Node A', to: 'Leaf 1'},
                    {from: 'Node A', to: 'Leaf 2'},
                    {from: 'Node B', to: 'Leaf 2'},
                    {from: 'Node C', to: 'Leaf 3'},
                ],
            }],
        };

        return (
            <div>
                <h3>Basic Charts</h3>
                <button onClick={this.changeInterval}>Randomize Interval</button>
                <HighchartsReact highcharts={Highcharts} options={optionsLine}/>
                <div style={{display: 'inline-block'}}>
                    <HighchartsReact highcharts={Highcharts} options={optionsBar}/>
                </div>
                <div style={{display: 'inline-block'}}>
                    <HighchartsReact highcharts={Highcharts} options={optionsBox}/>
                </div>
                <div style={{display: 'inline-block'}}>
                    <HighchartsReact highcharts={Highcharts} options={optionsPie}/>
                </div>

                <h3>Correlated Charts</h3>
                {/* TODO? */}
                <a href="https://www.highcharts.com/demo/synchronized-charts">Click here</a>

                <h3>Special Charts</h3>
                <div style={{display: 'inline-block'}}>
                    <HighchartsReact highcharts={Highcharts} options={optionsSankey}/>
                </div>
                <div style={{display: 'inline-block'}}>
                    <HighchartsReact highcharts={Highcharts} options={optionsChord}/>
                </div>
                <div style={{display: 'inline-block'}}>
                    <HighchartsReact highcharts={Highcharts} options={optionsNetwork}/>
                </div>

                <h3>Maps</h3>
                {/*TODO*/}
                <HighchartMap subplotSize={subplotWidth}/>

                <h3>Mass Test</h3>
                <HighchartBig/>
            </div>
        );
    }
}

export default Highchart;
