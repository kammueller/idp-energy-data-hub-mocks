import React from "react";
import {
    Line,
    CartesianGrid,
    XAxis,
    YAxis,
    Tooltip,
    BarChart,
    Bar,
    Legend,
    ResponsiveContainer,
    Brush,
    Area,
    ComposedChart,
    PieChart,
    Pie,
    Cell,
    ErrorBar, Sankey, Layer, Rectangle, LineChart, AreaChart,
} from 'recharts';
import {calculateConf, createBigData} from './utils';

type DemoLinkProps = {
    sourceX?: number,
    targetX?: number,
    sourceY?: number,
    targetY?: number,
    sourceControlX?: number,
    targetControlX?: number,
    sourceRelativeY?: number,
    targetRelativeY?: number,
    linkWidth?: number,
    index?: number,
}

class DemoLink extends React.Component<DemoLinkProps, any> {

    static displayName = 'SankeyLinkDemo';

    state = {
        fill: 'url(#linkGradient)',
    };

    render() {
        const {
            sourceX, targetX,
            sourceY, targetY,
            sourceControlX, targetControlX,
            sourceRelativeY, targetRelativeY,
            linkWidth,
            index,
        } = this.props;
        const {fill} = this.state;


        return (
            <Layer key={`CustomLink${index}`}>
                <path

                    d={`
            M${sourceX},${sourceY! + linkWidth! / 2}
            C${sourceControlX},${sourceY! + linkWidth! / 2}
              ${targetControlX},${targetY! + linkWidth! / 2}
              ${targetX},${targetY! + linkWidth! / 2}
            L${targetX},${targetY! - linkWidth! / 2}
            C${targetControlX},${targetY! - linkWidth! / 2}
              ${sourceControlX},${sourceY! - linkWidth! / 2}
              ${sourceX},${sourceY! - linkWidth! / 2}
            Z
          `}
                    fill={fill}
                    strokeWidth="0"
                    onMouseEnter={() => {
                        this.setState({fill: 'rgba(0, 136, 254, 0.5)'});
                    }}
                    onMouseLeave={() => {
                        this.setState({fill: 'url(#linkGradient)'});
                    }}
                />
            </Layer>
        );
    }
}

function DemoSankeyNode({x, y, width, height, index, payload, containerWidth}: any) {
    const isOut = x + width + 6 > containerWidth;
    return (
        <Layer key={`CustomNode${index}`}>
            <Rectangle
                x={x} y={y} width={width} height={height}
                fill="#5192ca" fillOpacity="1"
            />
            <text
                textAnchor={isOut ? 'end' : 'start'}
                x={isOut ? x - 6 : x + width + 6}
                y={y + height / 2}
                fontSize="14"
                stroke="#333"
            >{payload.name}</text>
            <text
                textAnchor={isOut ? 'end' : 'start'}
                x={isOut ? x - 6 : x + width + 6}
                y={y + height / 2 + 13}
                fontSize="12"
                stroke="#333"
                strokeOpacity="0.5"
            >{payload.value + 'k'}</text>
        </Layer>
    );
}

type RechartsProps = {};

type RechartsState = {
    selectedLines: Set<string>;
    intervalData: any[];
    yValuesBig: Array<any>;
    interpolation: number;
};

export default class Recharts extends React.Component<RechartsProps, RechartsState> {
    bigData = createBigData();
    countries: string[];

    constructor(props: Readonly<RechartsProps> | RechartsProps) {
        super(props);
        let lines = new Set<string>();
        lines.add('uv');
        lines.add('pv');

        let lineData = [
            {
                name: 'Page A',
                uv: 4000,
                uv_c: [4000, 4000],
                pv: 2400,
                amt: 2400,
                amt_err: [100, 100],
            },
            {
                name: 'Page B',
                uv: 3000,
                uv_c: [3000, 3000],
                pv: 1398,
                amt: 2210,
                amt_err: [20, 500],
            },
            {
                name: 'Page C',
                uv: 2000,
                uv_c: [2000, 2000],
                pv: 9800,
                amt: 2290,
                amt_err: [500, 100],
            },
            {
                name: 'Page D',
                uv: 2780,
                uv_c: [2780, 2780],
                pv: 3908,
                amt: 2000,
            },
            {
                name: 'Page E',
                uv: 1890,
                uv_c: [1500, 2000],
                pv: 4800,
                amt: 2181,
            },
            {
                name: 'Page F',
                uv: 3490,
                uv_c: [3190, 3790],
                pv: 3800,
                amt: 2500,
            },
            {
                name: 'Page G',
                uv: 2390,
                uv_c: [1790, 2990],
                pv: 4300,
                amt: 2100,
            },
        ];
        let uv = lineData.map(x => x.uv);
        let uv_c = calculateConf(uv);
        let new_data = lineData.map((x, index) => ({...x, uv_c: uv_c[index]}));

        let parsed: any[] = [];
        for (let d = 0; d < this.bigData[0].values.length; d++) {
            let date = this.bigData[0].values[d].when;
            let obj: any = {time: date};
            for (let c = 0; c < this.bigData.length; c++) {
                let country = this.bigData[c];
                obj[country.who] = country.values[d].what[0];
            }
            parsed.push(obj);
        }

        this.countries = this.bigData.map(c => c.who);

        this.state = {
            selectedLines: lines,
            intervalData: new_data,
            yValuesBig: parsed,
            interpolation: 0,
        };
    }

    // we have to do this ourselves...
    handleLegendClick = (o: any) => {
        const dataKey = o.dataKey;
        const lines = this.state.selectedLines;


        if (this.state.selectedLines.has(dataKey)) {
            lines.delete(dataKey);
            this.setState({selectedLines: lines});
        } else {
            lines.add(dataKey);
            this.setState({selectedLines: lines});
        }
    };

    changeInterval = () => {
        let uv = this.state.intervalData.map(x => x.uv);
        uv[uv.length - 2] = Math.round(5000 * Math.random());
        uv[uv.length - 1] = Math.round(3000 * Math.random());
        let uv_c = calculateConf(uv);

        let new_data = this.state.intervalData.map((x, index) => ({...x, uv: uv[index], uv_c: uv_c[index]}));

        this.setState({
            intervalData: new_data,
        });
    };

    changeBigData = (e: any) => {
        const newVal = e.target.value;

        let parsed: any[] = [];
        for (let d = 0; d < this.bigData[0].values.length; d++) {
            let date = this.bigData[0].values[d].when;
            let obj: any = {time: date};
            for (let c = 0; c < this.bigData.length; c++) {
                let country = this.bigData[c];
                obj[country.who] = (100 - newVal) / 100 * country.values[d].what[0]
                    + newVal / 100 * country.values[d].what[1];
            }
            parsed.push(obj);
        }

        this.setState({interpolation: newVal, yValuesBig: parsed});
    };

    renderCustomBarLabel = ({/*payload,*/ x, y, width, /*height,*/ value}: any) => {
        return <text x={x + width / 2} y={y} fill="#666" textAnchor="middle"
                     dy={-6}>{`${value.toFixed(0)}`}</text>;
    };

    render() {
        // color brewer dark2
        const COLORS = [
            'rgb(27, 158, 119)',
            'rgb(217, 95, 2)',
            'rgb(117, 112, 179)',
            'rgb(231, 41, 138)',
            'rgb(102, 166, 30)',
            'rgb(230, 171, 2)',
            'rgb(166, 118, 29)',
            'rgb(102, 102, 102)',
        ];

        const sankeyData = {
            "nodes": [
                {
                    "name": "Visit",
                },
                {
                    "name": "Direct-Favourite",
                },
                {
                    "name": "Page-Click",
                },
                {
                    "name": "Detail-Favourite",
                },
                {
                    "name": "Lost",
                },
            ],
            "links": [
                {
                    "source": 0,
                    "target": 1,
                    "value": 3728.3,
                },
                {
                    "source": 0,
                    "target": 2,
                    "value": 354170,
                },
                {
                    "source": 2,
                    "target": 3,
                    "value": 62429,
                },
                {
                    "source": 2,
                    "target": 4,
                    "value": 291741,
                },
            ],
        };

        return (
            <div>
                <h3>Basic Charts</h3>

                <button onClick={this.changeInterval}>Randomize Interval</button>

                <ResponsiveContainer width="100%" height={300}>
                    <ComposedChart width={600} height={300} data={this.state.intervalData}
                                   margin={{top: 5, right: 20, bottom: 5, left: 0}}>
                        <CartesianGrid stroke={'#ccc'} strokeDasharray={'5 5'}/>
                        <XAxis dataKey={'name'}/>
                        <YAxis yAxisId={'left'} domain={[0, 8000]}/>
                        <YAxis yAxisId={'right'} orientation={'right'} domain={[0, 'dataMax+1000']}/>
                        <Tooltip/>
                        <Legend onClick={this.handleLegendClick}/>

                        <Line yAxisId={'left'} type="natural" name={'Some Spline Data'} dataKey={'uv'}
                              style={this.state.selectedLines.has('uv') ? {} : {display: 'none'}}
                              stroke="#82ca9d" strokeDasharray={'5 2'}/>
                        <Area yAxisId={'left'} type="natural" name={'Spline Data - 95% conf.int.'} dataKey={'uv_c'}
                              style={this.state.selectedLines.has('uv') ? {} : {display: 'none'}} legendType={'none'}
                              stroke="none" fill="rgba(130,202,157,0.5)"/>
                        <Line yAxisId={'right'} type="linear" name={'Linear Data on other Axis'} dataKey={'pv'}
                              style={this.state.selectedLines.has('pv') ? {} : {display: 'none'}}
                              stroke="#8884d8" activeDot={{r: 8}}/>
                        <Brush/>
                    </ComposedChart>
                </ResponsiveContainer>

                <PieChart width={600} height={300} style={{display: 'inline-block'}}>
                    <Pie data={this.state.intervalData} dataKey="amt" outerRadius={60} fill="#8884d8" label>
                        {this.state.intervalData.map((entry, index) => ( // ugly but necessary
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]}/>
                        ))}
                    </Pie>
                    <Legend/>
                </PieChart>


                <BarChart width={600} height={300} data={this.state.intervalData} style={{display: 'inline-block'}}>
                    <XAxis dataKey={'name'}/>
                    <YAxis/>
                    <Bar dataKey={'uv'} barSize={30} fill="#8884d8" label={this.renderCustomBarLabel}/>
                    <Bar dataKey={'amt'} barSize={30} fill="#82ca9d" label={this.renderCustomBarLabel}>
                        <ErrorBar dataKey="amt_err" width={4} strokeWidth={2} stroke="#888"/>
                    </Bar>
                    <Legend/>
                </BarChart>

                <h3>Correlated Charts</h3>
                <ResponsiveContainer width="100%" height={200}>
                    <LineChart
                        width={500}
                        height={200}
                        data={this.state.intervalData}
                        syncId="anyId"
                        margin={{
                            top: 10,
                            right: 30,
                            left: 0,
                            bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <Tooltip/>
                        <Line type="monotone" dataKey="uv" stroke="#8884d8" fill="#8884d8"/>
                    </LineChart>
                </ResponsiveContainer>
                <p>The ghraphs are correlated</p>

                <ResponsiveContainer width="100%" height={200}>
                    <LineChart
                        width={500}
                        height={200}
                        data={this.state.intervalData}
                        syncId="anyId"
                        margin={{
                            top: 10,
                            right: 30,
                            left: 0,
                            bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <Tooltip/>
                        <Line type="monotone" dataKey="pv" stroke="#82ca9d" fill="#82ca9d"/>
                    </LineChart>
                </ResponsiveContainer>

                <ResponsiveContainer width="100%" height={200}>
                    <AreaChart
                        width={500}
                        height={200}
                        data={this.state.intervalData}
                        syncId="anyId"
                        margin={{
                            top: 10,
                            right: 30,
                            left: 0,
                            bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <Tooltip/>
                        <Area type="monotone" dataKey="pv" stroke="#82ca9d" fill="#82ca9d"/>
                        <Brush/>
                    </AreaChart>
                </ResponsiveContainer>

                <h3>Sankey</h3>
                <Sankey
                    width={960}
                    height={500}
                    data={sankeyData}
                    nodeWidth={10}
                    nodePadding={50}
                    link={<DemoLink/>}
                    node={<DemoSankeyNode containerWidth={960}/>}
                    margin={{
                        left: 200,
                        right: 200,
                        top: 100,
                        bottom: 100,
                    }}
                >
                    <defs>
                        <linearGradient id={'linkGradient'}>
                            <stop offset="0%" stopColor="rgba(0, 136, 254, 0.5)"/>
                            <stop offset="100%" stopColor="rgba(0, 197, 159, 0.3)"/>
                        </linearGradient>
                    </defs>
                    <Tooltip/>
                </Sankey>

                <h3>Mass Test</h3>
                <input type={'number'} min={0} max={100} step={5}
                       value={this.state.interpolation} onChange={this.changeBigData}/>
                <ResponsiveContainer width="100%" height={500}>
                    <ComposedChart width={600} height={300} data={this.state.yValuesBig}
                                   margin={{top: 5, right: 20, bottom: 5, left: 0}}>
                        <CartesianGrid/>
                        <XAxis dataKey={'time'} tickFormatter={(t: Date) => t.toDateString()}/>
                        <YAxis/>
                        <Tooltip/>

                        {
                            this.countries.map((c, i) => (
                                <Line key={c} type="natural" dataKey={c} stroke={COLORS[i % COLORS.length]}
                                      dot={false}/>
                            ))
                        }
                        <Brush/>
                        <Legend/>
                    </ComposedChart>
                </ResponsiveContainer>
            </div>
        );
    }
}
